import { Module } from '@nestjs/common';
import {MongoConfiguration} from "./Configurations/MongoConfiguration";
import {MiddlewareModule} from "./Bundles/MiddlewareBundle/middleware.module";
import {RiderModule} from "./Bundles/RiderBundle/rider.module";
@Module({
  imports: [MiddlewareModule,RiderModule],
  controllers: [],
  providers: [...MongoConfiguration],
  exports : [...MongoConfiguration]
})
export class AppModule {}
