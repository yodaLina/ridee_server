import {ConnectionOptions, createConnection} from 'typeorm';
import {async} from "rxjs/internal/scheduler/async";

export const MongoConfiguration = [{
  provide:'DATABASE_CONNECTION',
  useFactory:async () => await createConnection({
    type:"mongodb",
    host:"localhost",
    port: 27017,
    database : "ridee",
    entities: ["dist/**/*.entity{.ts,.js}"],
    useUnifiedTopology: true
})
}];
