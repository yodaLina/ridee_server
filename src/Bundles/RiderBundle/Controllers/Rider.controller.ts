import { RiderService } from '../Services/Rider.service';
import {Controller, Get, Post, UseGuards, Request, Body} from "@nestjs/common";
import { UserRiderDto } from '../DataTransferObjects/UserRiderDto';

@Controller()
export class RiderController {
    public constructor(private readonly riderService: RiderService){

    }

    @Post("suscribeRider")
    public async suscribeRider(@Body() riderDTO : UserRiderDto){
        let data = null
        let message = null;
        let status = 0;
        try {
            message = this.riderService.suscribeRider(riderDTO);
        } catch (error) {
            status = 1;
            message = error;
        }
        finally{
            return {
                data,
                message,
                status
            }
        }
    }
}
