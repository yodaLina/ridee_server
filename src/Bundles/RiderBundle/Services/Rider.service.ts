import {Inject, Injectable} from "@nestjs/common";
import {Repository} from "typeorm";
import {User} from "../../MiddlewareBundle/Entities/User.entity";
import { UserRiderDto } from "../DataTransferObjects/UserRiderDto";
import { InfobaseSubdocument } from "src/Bundles/MiddlewareBundle/Entities/Infobase.subdocument";
@Injectable
export class RiderService{
    constructor(@Inject("USER_REPOSITORY")private readonly userprovider : Repository<User>){

    }

    public async suscribeRider(riderDto : UserRiderDto) : Promise<string> {
        let rider = riderDto.toUser();
        let existingRider = this.userprovider.createQueryBuilder("user")
        .where("user._phonenumber = :phoneNumber or user._infobase._mail = :mail or user._infobase._nCIN = :nCIN or user._infobase._npasseport = :nPasseport", { phoneNumber : rider.phonenumber, mail : rider.infobase.mail, nCIN : rider.infobase.nCIN, nPasseport : rider.infobase.npassport})
        .getOne();

        if(existingRider == null ){
            this.userprovider.save(rider);
        }
        else{
            let existingRole = existingRider.role.find(r => r.nomRole = "Rider");
            if(existingRole != null){
                return "Vous avez déja un compte Rider ";
            }
            else{
                existingRider.role.push(rider.role[0]);
                this.userprovider.save(existingRider);
            }
        }

        return "Inscription réussie";
    }

}