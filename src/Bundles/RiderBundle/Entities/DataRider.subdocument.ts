import { DataUser } from "../../MiddlewareBundle/Entities/DataUser.subdocument";
import {Column} from "typeorm";

export class DataRiderSubdocument extends DataUser{
    @Column()
    private _marque : string;
    @Column()
    private _motoImage : string;
    @Column()
    private _matricule : string;
    
    get marque() : string{
        return this._marque;
    }

    set marque(value : string){
        this._marque = value;
    }

    get motoImage() : string {
        return this._motoImage;
    }

    set motoImage(value : string) {
        this.motoImage = value;
    }

    get matricule() : string {
        return this._matricule;
    }

    set matricule(value : string){
        this._matricule = value;
    }
}