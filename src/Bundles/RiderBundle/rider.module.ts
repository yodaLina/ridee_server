import {Module} from "@nestjs/common";
import { RiderController } from "./Controllers/Rider.controller";


@Module({
    imports: [
    ],
    controllers: [RiderController],
    providers: [],
    exports : []
})
export class RiderModule {

}