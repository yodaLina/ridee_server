import { IsEmail, IsNotEmpty, IsNumberString, IsDate} from 'class-validator';
import { User } from 'src/Bundles/MiddlewareBundle/Entities/User.entity';
import { InfobaseSubdocument } from 'src/Bundles/MiddlewareBundle/Entities/Infobase.subdocument';
import { RoleSubdocument } from 'src/Bundles/MiddlewareBundle/Entities/Role.subdocument';
import { DataRiderSubdocument } from '../entities/DataRider.subdocument';
export class UserRiderDto{
    @IsNotEmpty
    @IsNumberString
    phoneNumber : string;
    @IsNotEmpty
    password : string;
    @IsNotEmpty
    fullName : string;
    nationalite : string;
    nCIN : string;
    imageProfil : string;
    imageDoc : string;
    adresse : string;
    @IsEmail
    mail : string;
    dateNaissance : Date;
    nPasseport : string;
    sexe : string;
    marque : string;
    motoImage : string;
    matricule :string;

    toUser(){
        let user: User = new User();
        user.phonenumber = this.phoneNumber;
        user.password = this.password;
        user.infobase = new InfobaseSubdocument();
        user.infobase.fullName = this.fullName;
        user.infobase.nationalite = this.nationalite;
        user.infobase.datenaissance = this.dateNaissance;
        user.infobase.sexe = this.sexe;
        user.infobase.adresse = this.adresse;
        user.infobase.mail = this.mail;
        user.infobase.nCIN = this.nCIN;
        user.infobase.npassport = this.nPasseport;
        user.infobase.imageprofil = this.imageProfil;
        user.infobase.imagedoc = this.imageDoc;
        user.role = new Array<RoleSubdocument>();

        let role = new RoleSubdocument();
        role.nomRole = "Rider";
        let dataRider = new DataRiderSubdocument();
        dataRider.marque = this.marque;
        dataRider.matricule = this.matricule;
        dataRider.motoImage = this.motoImage;
        role.data = dataRider;

        user.role.push(role);
        return user;
    }
}