import {Inject, Injectable} from "@nestjs/common";
import {Repository} from "typeorm";
import {User} from "../Entities/User.entity";
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import {jwtConstants} from "../Configurations/constants";
import {SignOptions} from "jsonwebtoken";

@Injectable()
export class UserService{

    private readonly tokenexpiration: number = 60*36000;
    constructor(@Inject("USER_REPOSITORY")private readonly userprovider : Repository<User>, private readonly jwtService: JwtService){

    }
    public async validateUser(phonenumber:string,password:string): Promise<User | undefined>{
        let utilisateur: User =await this.userprovider.findOne({where:{_phonenumber:phonenumber}});
        if(utilisateur !== undefined && utilisateur!==null){
            if(await this.compareHash(password,utilisateur.password)){
                return utilisateur;
            }
        }
        return null;
    }
    public async login(user:User): Promise<any>{
        const payload = {username:user.phonenumber,sub:user.id.toString()};
        return {
            expiration: this.tokenexpiration,
            access_token: this.jwtService.sign(payload,{expiresIn:this.tokenexpiration})
        };
    }
    public async createRandomUser(phonenumber:string, password:string){
        let user:User = new User();
        user.phonenumber = phonenumber;
        user.password = await this.gethash(password);
        this.userprovider.save(user);
    }
    protected async gethash(password:string): Promise<string>{
        return bcrypt.hash(password,10);
    }
    protected compareHash(password: string|undefined, hash: string|undefined): Promise<boolean> {
        return bcrypt.compare(password, hash);
    }
}