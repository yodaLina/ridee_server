import {Controller, Get, Post, UseGuards, Request, Body} from "@nestjs/common";
import {UserService} from "../Services/User.service";
import {LocalAuthGuard} from "../RouteGuards/Guards/local-auth.guard";
import {RequestQueueSizeEvent} from "@nestjs/common/interfaces/external/kafka-options.interface";

@Controller("user")
export class UserController{
    public constructor(private readonly userservice: UserService){

    }
    @UseGuards(LocalAuthGuard)
    @Post("auth/login")
    public async login(@Request() req){
        let data = null;
        let message = "Succès";
        let status = 0;
        try{
            data=  this.userservice.login(req.user);
        }
        catch (e) {
            status = 1;
            message = e;
        }
        finally {
            return {
                data,
                message,
                status
            };
        }

    }

    @Post("inscription")
    public async generateUser(@Body("username")username: string,@Body("password")password: string){
        let data = null;
        let message = "Succès";
        let status = 0;
        try{
            await this.userservice.createRandomUser(username,password);
        }
        catch (e) {
            status = 1;
            message = e;
        }
        finally {
            return {
              data,
              message,
              status
            };
        }
    }
}