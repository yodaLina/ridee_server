import {forwardRef, Module} from '@nestjs/common';
import {UserProvider} from "./Repositories/providers/User.provider";
import {MongoConfiguration} from "../../Configurations/MongoConfiguration";
import {AppModule} from "../../app.module";
import {UserController} from "./Controllers/User.controller";
import {UserService} from "./Services/User.service";
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import {jwtConstants} from "./Configurations/constants";
import {LocalStrategy} from "./RouteGuards/Strategies/local.strategy";
import {JwtStrategy} from "./RouteGuards/Strategies/jwt.strategy";

@Module({
    imports: [
        forwardRef(()=>AppModule),
        PassportModule.register({ defaultStrategy: 'jwt' }),
        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions: { expiresIn: '600000000s' },
        })
    ],
    controllers: [UserController],
    providers: [...UserProvider,UserService,LocalStrategy,JwtStrategy],
    exports : []
})
export class MiddlewareModule {

}
