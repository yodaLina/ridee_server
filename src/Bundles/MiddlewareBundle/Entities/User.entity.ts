import {Column, Entity, ObjectID, ObjectIdColumn} from "typeorm";
import {InfobaseSubdocument} from "./Infobase.subdocument";
import {RoleSubdocument} from "./Role.subdocument";

@Entity()
export class User{
  get role(): RoleSubdocument[] {
    return this._role;
  }

  set role(value: RoleSubdocument[]) {
    this._role = value;
  }
  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  @ObjectIdColumn()
  private _id : ObjectID;

  @Column({name:"_phonenumber"})
  private _phonenumber: string;


  @Column({name:"_password"})
  private _password:string;

  @Column(type => InfobaseSubdocument)
  private _infobase: InfobaseSubdocument;

  @Column(type =>RoleSubdocument)
  private _role: RoleSubdocument[];

  get phonenumber(): string {
    return this._phonenumber;
  }

  set phonenumber(value: string) {
    this._phonenumber = value;
  }

  get infobase(): InfobaseSubdocument {
    return this._infobase;
  }

  set infobase(value: InfobaseSubdocument) {
    this._infobase = value;
  }

  get id(): ObjectID {
    return this._id;
  }

  set id(value: ObjectID) {
    this._id = value;
  }


  public constructor(){

  }
}
