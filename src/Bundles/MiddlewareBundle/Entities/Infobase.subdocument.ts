import {Column} from "typeorm";

export class InfobaseSubdocument{
    get fullName(): string {
        return this._fullName;
    }

    set fullName(value: string) {
        this._fullName = value;
    }

    get nationalite(): string {
        return this._nationalite;
    }

    set nationalite(value: string) {
        this._nationalite = value;
    }

    get nCIN(): string {
        return this._nCIN;
    }

    set nCIN(value: string) {
        this._nCIN = value;
    }

    get imageprofil(): string {
        return this._imageprofil;
    }

    set imageprofil(value: string) {
        this._imageprofil = value;
    }

    get imagedoc(): string {
        return this._imagedoc;
    }

    set imagedoc(value: string) {
        this._imagedoc = value;
    }

    get adresse(): string {
        return this._adresse;
    }

    set adresse(value: string) {
        this._adresse = value;
    }

    get mail(): string {
        return this._mail;
    }

    set mail(value: string) {
        this._mail = value;
    }

    get datenaissance(): Date {
        return this._datenaissance;
    }

    set datenaissance(value: Date) {
        this._datenaissance = value;
    }

    get npassport(): string {
        return this._npassport;
    }

    set npassport(value: string) {
        this._npassport = value;
    }

    get sexe(): string {
        return this._sexe;
    }

    set sexe(value: string) {
        this._sexe = value;
    }
    @Column({name:"_fullName"})
    private _fullName: string;

    @Column({name:"_nationalite"})
    private _nationalite: string;

    @Column({name:"_nCIN"})
    private _nCIN: string;

    @Column({name:"_imageprofil"})
    private _imageprofil: string;

    @Column({name:"_imagedoc"})
    private _imagedoc: string;

    @Column({name:'_adresse'})
    private _adresse: string;

    @Column({name:"_mail"})
    private _mail: string;

    @Column({name:"_datenaissance"})
    private _datenaissance: Date;

    @Column({name:"_npassport"})
    private _npassport: string;

    @Column({name:"_sexe"})
    private _sexe: string;





}