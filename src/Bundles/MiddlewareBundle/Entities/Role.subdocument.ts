import { DataUser } from "./DataUser.subdocument";
import {Column} from "typeorm";
import {DateUtils} from "typeorm/util/DateUtils";

export class RoleSubdocument{
    @Column()
    private _nomRole : string;

    @Column(type => DateUtils)
    private _data : DataUser;

    get nomRole() : string {
        return this._nomRole;
    }
    
    set nomRole(value : string){
        this._nomRole = value;
    }

    get data() : DataUser{
        return this._data;
    }

    set data(value : DataUser){
        this._data = value;
    }


    
}